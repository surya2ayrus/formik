import { Formik, Form, Field, ErrorMessage, FieldArray, FastField } from 'formik'
import React from 'react'
import * as Yup from 'yup';
import ErrorText from './ErrorText';

const initialValues = {
    name: '',
    email: '',
    channel: '',
    address: '',
    social: {
        facebook: '',
        twitter: ''
    },
    phoneNumbers: ['', ''],
    phNumbers: ['']
}

const onSubmit = (values) => {
    console.log('on submit', values);
}

const validationSchema = Yup.object({
    name: Yup.string().trim().required('Required!'),
    email: Yup.string().email('Invalid Format').required('Required!'),
    channel: Yup.string().trim().required('Required!'),
    address: Yup.string().required('Required!'),
    social: Yup.object({
        facebook: Yup.string().required('Required!'),
        twitter: Yup.string().required('Required!')
    }),
    phoneNumbers: Yup.array(Yup.string().required('Required!'), Yup.string().required('Required!'))
})

function YoutubeForm() {
    // console.log('form values');
    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
        >
            <Form>
                <div className='form-control'>
                    <label htmlFor='name'>Name</label>
                    <Field
                        type='text'
                        id='name'
                        name='name'
                    />
                    {/* Displaying Error in Component Format Using Formik */}
                    <ErrorMessage name='name' component={ErrorText} />
                </div>

                <div className='form-control'>
                    <label htmlFor='email'>Email</label>
                    <Field
                        type='email'
                        id='email'
                        name='email'
                    />
                    {/* Display Error using Render Props */ }
                    <ErrorMessage name='email'>
                        {
                            (errorMsg) => {
                                return (
                                    <div className='error'>{errorMsg}</div>
                                )
                            }
                        }
                    </ErrorMessage>
                </div>
                <div className='form-control'>
                    <label htmlFor='channel'>Channel</label>
                    <Field
                        type='text'
                        id='channel'
                        name='channel'
                        placeholder='YouTube channel name'
                    />
                    <ErrorMessage name='channel' />
                </div>
                <div className='form-control'>
                    <label htmlFor='address'> Address </label>
                    <FastField name='address'>
                        {
                            props => {
                                const { field, meta } = props;
                                console.log('props', props);
                                return (
                                    <div>
                                        <input id='address' type='text' {...field} />
                                        {meta.error && meta.touched ? <div className='error'>{meta.error}</div> : null}
                                    </div>
                                )
                            }
                        }
                    </FastField>
                </div>

                <div className='form-control'>
                    <label htmlFor='facebook'> Facebook </label>
                    <Field name='social.facebook' type='text' id='facebook' />
                    <ErrorMessage name='social.facebook' component={ErrorText}/>
                </div>
                
                <div className='form-control'>
                    <label htmlFor='twitter'> Twitter </label>
                    <Field name='social.twitter' type='text' id='twitter' />
                    <ErrorMessage name='social.twitter' component={ErrorText}/>
                </div>

                <div className='form-control'>
                    <label htmlFor='Primary Phone Number'>Primary Phone Number</label>
                    <Field name='phoneNumbers[0]' id='primaryPh' type='text' />
                    <ErrorMessage name='phoneNumbers[0]' component={ErrorText} />
                </div>

                <div className='form-control'>
                    <label htmlFor='Secondary Phone Number'>Secondary Phone Number</label>
                    <Field name='phoneNumbers[1]' id='secondaryPh' type='text' />
                    <ErrorMessage name='phoneNumbers[1]' component={ErrorText} />
                </div>

                <div className='form-control'>
                    <label htmlFor='List of Phone Numbers'>List of Phone Numbers</label>
                    <FieldArray name='phNumbers' id='phNumbers'>
                        {
                            (fieldArrayProps) => {
                                const { push, remove, form } = fieldArrayProps;
                                const { phNumbers } = form.values;
                                return (
                                    <div>
                                        {
                                            phNumbers.map((phNumber, index) => {
                                                return (
                                                    <div key={index}>
                                                        <Field id={`phNumber-${index}`} name={`phNumbers[${index}]`} />
                                                        <button type='button' onClick={() => push('')}>+</button>
                                                        <button type='button' onClick={() => remove(index)} disabled={index < 1}>-</button>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                )
                            }
                        }
                    </FieldArray>
                </div>

                <button type='submit'>Submit</button>
            </Form>
        </Formik>
    )
}

export default YoutubeForm