import { ErrorMessage, Field } from 'formik'
import React from 'react'
import ErrorText from './ErrorText';

function RadioButtons(props) {
    const { name, label, options, ...rest } = props;
    return (
        <div className='form-control'>
            <label htmlFor={name}>{label}</label>
            <Field name={name} {...rest}>
                {
                    ({ field }) => {
                        return options.map((option) => {
                            return (
                                <React.Fragment key={option.value}>
                                    <input
                                        type='radio'
                                        id={option.value}
                                        {...field}
                                        checked={field.value == option.value}
                                        value={option.value}
                                        {...rest}
                                    />
                                    <label htmlFor={option.value}>{option.key}</label>
                                </React.Fragment>
                            )
                        })
                    }
                }
            </Field>
            <ErrorMessage name={name} component={ErrorText} />
        </div>
    )
}

export default RadioButtons