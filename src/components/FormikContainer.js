import React from 'react'
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import FormikController from './FormikController';

function FormikContainer() {
    const initialValues = {
        email: '',
        description: '',
        selectOption: '',
        radioOption: '',
        checkboxOption: [],
        birthDate: null
    };
    const validationSchema = Yup.object({
        email: Yup.string().required('Required!'),
        description: Yup.string().required('Required!'),
        selectOption: Yup.string().required('Required!'),
        radioOption: Yup.string().required('Required!'),
        checkboxOption: Yup.array().required('Required'),
        birthDate: Yup.date().required('Required!').nullable()
    });
    const onSubmit = (values) => {
        console.log('submit values', values);
    };
    const dropdownOptions = [
        { key: 'Select an option', value: '' },
        { key: 'Option 1', value: 'option1' },
        { key: 'Option 2', value: 'option2' },
        { key: 'Option 3', value: 'option3' },
    ]
    const radioOptions = [
        { key: 'R Option 1', value: 'rOption1' },
        { key: 'R Option 2', value: 'rOption2' },
        { key: 'R Option 3', value: 'rOption3' },
    ]
    const checkboxOptions = [
        { key: 'CB Option 1', value: 'cbOption1' },
        { key: 'CB Option 2', value: 'cbOption2' },
        { key: 'CB Option 3', value: 'cbOption3' },
    ]
    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
        >
            {
                (formik) => {
                    return (
                        <Form>
                            <FormikController
                                control='input'
                                type='email'
                                name='email'
                                label='Email'
                            />
                            <FormikController
                                control='textarea'
                                type='textarea'
                                name='description'
                                label='description'
                            />
                            <FormikController
                                control='select'
                                name='selectOption'
                                label='Select a topic'
                                options={dropdownOptions}
                            />
                            <FormikController
                                control='radio'
                                name='radioOption'
                                label='Radio Topics'
                                options={radioOptions}
                            />
                            <FormikController
                                control='checkbox'
                                label='Checkbox topics'
                                name='checkboxOption'
                                options={checkboxOptions}
                            />
                            <FormikController
                                control='date'
                                label='Pick a date'
                                name='birthDate'
                            />
                            <button type='submit'>Submit</button>
                        </Form>
                    )
                }
            }
        </Formik>
    )
}

export default FormikContainer