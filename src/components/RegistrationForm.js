import { Form, Formik } from 'formik';
import React from 'react'
import * as Yup from 'yup';
import FormikController from './FormikController';

function RegistrationForm() {
    const initialValues = {
        email: '',
        password: '',
        confirmPassword: '',
        modeOfContact: '',
        phone: ''
    };
    const options = [
        { key: 'Email', value: 'email' },
        { key: 'Telephone', value: 'telephonemoc' }
    ];
    const validationSchema = Yup.object({
        email: Yup.string().email('Invalid Format').required('Required'),
        password: Yup.string().required('Required'),
        confirmPassword: Yup.string().oneOf([Yup.ref('password'), ''], 'Password must match').required('Required'),
        modeOfContact: Yup.string().required('Required'),
        phone: Yup.string().when('modeOfContact', {
            is: 'telephonemoc',
            then: Yup.string().required('Required')
        })
    });
    const onSubmit = (values) => {
        console.log('submitted', values);
    }

    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
            validateOnChange={false}
        >
            {
                (formik) => {
                    return (
                        <Form>
                            <FormikController
                                control='input'
                                name='email'
                                label='Email'
                                type='email'
                            />
                            <FormikController
                                control='input'
                                name='password'
                                type='password'
                                label='Password'
                            />
                            <FormikController
                                control='input'
                                name='confirmPassword'
                                type='password'
                                label='Confirm Password'
                            />
                            <FormikController
                                control='radio'
                                options={options}
                                name='modeOfContact'
                                label='Mode Of Contact'
                            />
                            <FormikController
                                control='input'
                                name='phone'
                                label='Phone No'
                                type='text'
                            />
                            <button type='submit'>Submit</button>
                        </Form>
                    )
                }
            }
        </Formik>
    )
}

export default RegistrationForm