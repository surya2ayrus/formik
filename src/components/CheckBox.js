import { ErrorMessage, Field } from 'formik'
import React from 'react'
import ErrorText from './ErrorText';

function CheckBox(props) {
    const { name, label, options, ...rest } = props;
    return (
        <div className='form-control'>
            <label htmlFor={name}>{label}</label>
            <Field name={name} {...rest}>
                {
                    ({ field }) => {
                        return options.map((option) => {
                            console.log('fields', field, option.value)
                            return (
                                <React.Fragment key={option.value}>
                                    <input
                                        type='checkbox'
                                        id={option.value}
                                        {...field}
                                        checked={field.value.includes(option.value)}
                                        value={option.value}
                                        {...rest}
                                    />
                                    <label htmlFor={option.value}>{option.key}</label>
                                </React.Fragment>

                            )
                        })
                    }
                }
            </Field>
            <ErrorMessage name={name} component={ErrorText} />
        </div>
    )
}

export default CheckBox