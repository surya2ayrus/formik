import React from 'react'
import FormikController from './FormikController'
import Input from './Input'
import * as Yup from 'yup';
import { Form, Formik } from 'formik';

function LoginForm(props) {
    const initialValues = {
        email: '',
        password: ''
    }
    const validationSchema = Yup.object({
        email: Yup.string().email('Invalid Format').required('Required'),
        password: Yup.string().required('Required')
    });
    const onSubmit = (props) => {
        console.log('submitted propss', props);
    }
    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={onSubmit}
        >
            {
                (formik) => {
                    return (
                        <Form>
                            <FormikController
                                control='input'
                                name='email'
                                label='Email'
                                type='email'
                            />
                            <FormikController
                                control='input'
                                name='password'
                                label='Password'
                                type='password'
                            />
                            <button
                                type='submit'
                                disabled={!formik.isValid}
                            >
                                Submit
                            </button>
                        </Form>
                    )
                }
            }

        </Formik>
    )
}

export default LoginForm