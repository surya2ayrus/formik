import { useFormik } from 'formik'
import React from 'react'
import * as Yup from 'yup';

const initialValues = {
    name: 'test',
    email: '',
    channel: ''
}

const onSubmit = (values) => {
    console.log('on submit', values);
}

const validationSchema = Yup.object({
    name: Yup.string().trim().required('Required!'),
    email: Yup.string().email('Invalid Format').required('Required!'),
    channel: Yup.string().trim().required('Required!')
})

const validate = (values) => {
    let errors = {};
    if (!values.name) {
        errors.name = "Required";
    }

    if (!values.email) {
        errors.email = "Required"
    } else if (!/^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$/i.test(values.email)) {
        errors.email = "Invalid Format";
    }
    if (!values.channel) {
        errors.channel = "Required";
    }
    // console.log('values', values);
    return errors
}

function OldYoutubeForm() {
    const formik = useFormik({
        initialValues,
        onSubmit,
        // validate
        validationSchema
    });
    console.log('form validation schema', validationSchema);
    return (
        <div>
            <form onSubmit={formik.handleSubmit}>
                <div className='form-control'>
                    <label htmlFor='name'>Name</label>
                    <input
                        type='text'
                        id='name'
                        name='name'
                        onChange={formik.handleChange}
                        value={formik.values.name}
                        onBlur={formik.handleBlur}
                    />
                    {formik.touched.name && formik.errors.name ? <div className='error'>{formik.errors.name}</div> : null}
                </div>

                <div className='form-control'>
                    <label htmlFor='email'>Email</label>
                    <input
                        type='email'
                        id='email'
                        name='email'
                        onChange={formik.handleChange}
                        value={formik.values.email}
                        onBlur={formik.handleBlur}
                    />
                    {formik.touched.email && formik.errors.email ? <div className='error'>{formik.errors.email}</div> : null}
                </div>
                <div className='form-control'>
                    <label htmlFor='channel'>Channel</label>
                    <input
                        type='text'
                        id='channel'
                        name='channel'
                        placeholder='YouTube channel name'
                        onChange={formik.handleChange}
                        value={formik.values.channel}
                        onBlur={formik.handleBlur}
                    />
                    {formik.touched.channel && formik.errors.channel ? <div className='error'>{formik.errors.channel}</div> : null}
                </div>

                <button type='submit'>Submit</button>
            </form>
        </div>
    )
}

export default OldYoutubeForm