import './App.css';
// import OldYoutubeForm from './components/OldYoutubeForm';
import YoutubeForm from './components/YoutubeForm';
import FormikContainer from './components/FormikContainer';
import LoginForm from './components/LoginForm';
import RegistrationForm from './components/RegistrationForm';

function App() {
  return (
    <div className="App">
      <RegistrationForm />
    </div>
  );
}

export default App;
